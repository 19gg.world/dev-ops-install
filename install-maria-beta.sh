#!/bin/bash

sudo apt update

sudo apt upgrade -y

sudo apt-get install -y apt-transport-https curl

sudo curl -o /etc/apt/trusted.gpg.d/mariadb_release_signing_key.asc 'https://mariadb.org/mariadb_release_signing_key.asc'

sudo sh -c "echo 'deb https://mirror.kku.ac.th/mariadb/repo/10.6/ubuntu focal main' >/etc/apt/sources.list.d/mariadb-10.6.list"

sudo apt-get update
sudo wget http://archive.ubuntu.com/ubuntu/pool/main/o/openssl/libssl1.1_1.1.1f-1ubuntu2_amd64.deb

sudo dpkg -i libssl1.1_1.1.1f-1ubuntu2_amd64.deb
wget http://ftp.us.debian.org/debian/pool/main/r/readline5/libreadline5_5.2+dfsg-3+b13_amd64.deb

sudo dpkg -i libreadline5_5.2+dfsg-3+b13_amd64.deb
sudo apt-get install mariadb-server -y
sudo systemctl enable mariadb
sudo apt-get update -y

sudo apt-get install mariadb-server galera-4 mariadb-client libmariadb3 mariadb-backup mariadb-common -y

#------------------------------------#

service_file="/etc/systemd/system/multi-user.target.wants/mariadb.service"

if [ -f "$service_file" ]; then
    sudo sed -i 's/^LimitNOFILE=.*/LimitNOFILE=65536/' $service_file
    echo "ไฟล์ $service_file ได้ถูกแก้ไขเรียบร้อยแล้ว"
else
    echo "ไม่พบไฟล์ $service_file"
    exit 1
fi

systemctl daemon-reload
#------------------------------------#


config_file="/etc/mysql/mariadb.conf.d/50-server.cnf"

if [ -f "$config_file" ]; then
    sudo sed -i 's/^bind-address.*$/bind-address = 0.0.0.0/' $config_file
    echo " ___________"
    echo " \n"
    echo " \n"
else
    echo "หาไม่พบนิ : $config_file"
    exit 1
fi

#---
if [ -f "$config_file" ]; then

    sed -i '/^bind-address/a plugin_load_add = server_audit\nmax_connections = 10000\ninnodb_buffer_pool_size = 4G\nlog_error = /var/log/mysql/error.log\nmax_prepared_stmt_count = 50000\nslow_query_log\nslow_query_log_file = /var/log/mysql/mariadb-slow.log\nlong_query_time = 2' $config_file
    echo "เจ๋งแจ๋ว $config_file เรียบร้อยแล้ว"
else
    echo "ไม่เจอเบอะ หาใหม่ต่ะ: $config_file"
    exit 1
fi

#---

#!/bin/bash

read -p "ต้องการทำการแก้ไขไฟล์ galera.cluster หรือไม่? (y/n): " answer

if [ "$answer" = "y" ]; then
    echo "OK"
    read -p "Enter your Galera cluster name: " galera_cluster_name
    read -p "Enter IP Node - 01: " node_01
    read -p "Enter IP Node - 02: " node_02
    read -p "Enter IP Node - 03: " node_03
    read -p "VM IP: " server_ip
    read -p "VM hostname: " server_hostname

    cat <<EOT | sudo tee /etc/mysql/mariadb.conf.d/60-galera.cnf > /dev/null
[galera]
wsrep_on                 = ON

wsrep_cluster_name       = "$galera_cluster_name" 
#wsrep_cluster_address    = gcomm://
wsrep_provider           = /usr/lib/galera/libgalera_smm.so
wsrep_cluster_address    = gcomm://$node_01,$node_02,$node_03
binlog_format            = row

default_storage_engine   = Innodb
innodb_autoinc_lock_mode = 2

# Galera Node Configuration
wsrep_node_address="$server_ip" 
wsrep_node_name="$server_hostname" 

# Allow server to accept connections on all interfaces.
#bind-address = 0.0.0.0

# Optional settings
#wsrep_slave_threads = 1
#innodb_flush_log_at_trx_commit = 0
EOT

    echo "ไฟล์ /etc/mysql/mariadb.conf.d/60-galera.cnf ได้ถูกแก้ไขเรียบร้อยแล้ว"
elif [ "$answer" = "n" ]; then

    echo "ยกเลิก"
else

    echo "คำตอบไม่ถูกต้อง กรุณาใส่ 'y' หรือ 'n'"
fi
